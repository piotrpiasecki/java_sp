package Maps;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class SimpleHashMapTest
{
    private static final Car car1 = new Car(90, "green");
    private static final Car car2 = new Car(190, "red");
    private static final Car car3 = new Car(290, "black");

    private static final Entry<Integer, Car> car1Entry = new Entry<>(11, car1);
    private static final Entry<Integer, Car> car2Entry = new Entry<>(8, car2);
    private static final Entry<Integer, Car> car3Entry = new Entry<>(134, car3);

    private static final SimpleHashMap<Integer, Car> EMPTY_SIMPLEHASHMAP = new SimpleHashMap<>();



    @Test
    void shouldReturnFalseInCaseOfEmptySimpleHashMapWhileLookingForAKey()
    {
        assertFalse(
                EMPTY_SIMPLEHASHMAP.containsKey(
                        car1Entry.getKey()));
    }

    @Test
    void shouldReturnTrueIfContainsGivenKey()
    {
        SimpleHashMap<Integer, Car> sampleSimpleHashMap = new SimpleHashMap<>();
        sampleSimpleHashMap
                .put(car2Entry.getKey(), car2Entry.getValue());

        assertTrue(sampleSimpleHashMap.containsKey(car2Entry.getKey()));
    }

    @Test
    void shouldReturnFalseInCaseOfEmptySimpleHashMapAndLookingForAValue()
    {
        assertFalse(
                EMPTY_SIMPLEHASHMAP.containsValue(car3));
    }

    @Test
    void shouldReturnTrueIfContainsGivenValue()
    {
        SimpleHashMap<Integer, Car> anotherSampleSimpleHashMap = new SimpleHashMap<>();
        anotherSampleSimpleHashMap
                .put(car3Entry.getKey(), car3Entry.getValue());

        assertTrue(
                anotherSampleSimpleHashMap.containsValue(car3));
    }
}