package Shopping;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ItemTest
{

    private static final String MOCK_NAME_1 = "mock name 1";
    private static final String MOCK_NAME_2 = "mock name 2";
    private static final Double MOCK_PRICE_1 = 123.12;
    private static final Double MOCK_PRICE_2 = 321.32;

    @Test
    public void twoItemsWithTheSamePriceAndNameShouldBeEqual()
    {
        assertEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1),new Item(MOCK_NAME_1, MOCK_PRICE_1));
    }

    @Test
    public void itemsWithDifferentNamesShouldntBeEqual()
    {
        assertNotEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1),
                new Item(MOCK_NAME_2, MOCK_PRICE_1));
    }

    @Test
    public void itemsWithDifferentPricesShouldntBeEqual()
    {
        assertNotEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1),
                new Item(MOCK_NAME_1, MOCK_PRICE_2));
    }

    @Test
    public void itemsWithDifferentNamesAndPricesShouldntBeEqual()
    {
        assertNotEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1),
                new Item(MOCK_NAME_2, MOCK_PRICE_2));
    }

    @Test
    public void anyItemShouldntBeEqualToNull()
    {
        assertNotEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1), null);
    }

    @Test
    public void itemsWithIdenticalNamesShouldHaveIdenticalHashCodes()
    {
        assertEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1).hashCode(),
                new Item(MOCK_NAME_1, MOCK_PRICE_2).hashCode());
    }

    @Test
    public void itemsWithDifferentNamesShouldHaveDifferentHashCodes()
    {
        assertNotEquals(new Item(MOCK_NAME_1, MOCK_PRICE_1).hashCode(),
                new Item(MOCK_NAME_2, MOCK_PRICE_1).hashCode());
    }

    @Test
    public void itemsWithIdenticalNamesAndDifferentPricesShouldBeOrdered()
    {
        assertEquals(-1,
                new Item(MOCK_NAME_1, MOCK_PRICE_1)
                        .compareTo(new Item(MOCK_NAME_1, MOCK_PRICE_2))
        );
    }
}