package Weight;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

class UnitConverterTest
{
    int SCALE = 4;
    RoundingMode ROUNDING_MODE = RoundingMode.CEILING;

    @Test
    @DisplayName("0 pounds to kilograms ♥ ♦ ♣ ♠")
    void shouldConvertZeroKilogramValue()
    {
        Pound pounds = new Kilogram(BigDecimal.ZERO).toPounds();
        assertEquals(BigDecimal.ZERO.setScale(SCALE), pounds.value);
    }

    @Test
    void shouldConvertZeroPoundValue()
    {
        Kilogram kilograms = new Pound(BigDecimal.ZERO).toKilograms();
        assertEquals(BigDecimal.ZERO.setScale(SCALE), kilograms.value);
    }

    @Test
    void shouldConvertOneKilogramValue()
    {
        Pound pounds = new Kilogram(BigDecimal.ONE).toPounds();
        assertEquals(new BigDecimal("2.2046"), pounds.value);
    }

    @Test
    void shouldConvertOnePoundValue()
    {
        Kilogram kilograms = new Pound(BigDecimal.ONE).toKilograms();
        assertEquals(new BigDecimal("0.4536"), kilograms.value);
    }

    @Test
    void shouldntAcceptNegativeWeightInPounds() {
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> new Pound(new BigDecimal(-1))
        );
        assertEquals("Weight must be non-negative!", exception.getMessage());
    }

    @Test
    void shouldntAcceptNullValue() {
        assertAll(
                () -> assertThrows(NullPointerException.class, () -> new Kilogram(null)),
                () -> assertThrows(NullPointerException.class, () -> new Pound(null))
        );
    }
    @Test
    @ExtendWith(SamouczekExtension.class)
    void shouldConvertZeroPoundValue_2() {
        Kilogram kilograms = new Pound(BigDecimal.ZERO).toKilograms();
        assertEquals(BigDecimal.ZERO.setScale(4), kilograms.value);
    }

}