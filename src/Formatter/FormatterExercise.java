package Formatter;

import java.util.Formatter;
import java.util.Locale;

public class FormatterExercise
{
    public static void main(String[] args)
    {
        Formatter formatter = new Formatter();
        formatter.format("Samouczek programisty istnieje od %d roku. Wszystkie teksty pisze %s", 2015, "Marcin");
        String newString = formatter.toString();
        System.out.println(newString);

        System.out.format("Samouczek programisty istnieje od %d roku. Wszystkie teksty pisze %s", 2015, "Marcin");

        System.out.println();

        System.out.format("[%2$s] %1$s [%1$s]", "pierwszy", "drugi");

        System.out.println();

        System.out.format("[%10s] [%3s]", "test", "test");

        System.out.println();

        System.out.format("[%10s] [%-10s]", "test", "test");

        System.out.println();

        System.out.println(Locale.getDefault());

        double someNumber = 12345.12;
        System.out.format(Locale.US, "%,.2f%n", someNumber);
        System.out.format(Locale.GERMAN, "%,.2f%n", someNumber);
        System.out.format(Locale.forLanguageTag("PL"), "%,.2f%n", someNumber);
    }
}
