package Lambdas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class LambdaSortingApp
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        List<String> strings = new ArrayList<>();

        for (int i = 1; i <= 4; i++)
        {
            System.out.println("Podaj ciąg znaków: ");
            strings.add(input.nextLine());
        }

        strings.forEach(System.out::println);

        Comparator<String> stringComparator = Comparator.comparing(s -> - s.length());

        strings.sort(stringComparator);

        System.out.println();

        strings.forEach(System.out::println);
    }

}
