package Lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class NumbersPrinting
{
    public static void main(String[] args)
    {

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);

        System.out.println();

        for (Integer integer : numbers)
        {
            System.out.println(integer);
        }

        System.out.println();

        Consumer<Integer> integerConsumer = n -> System.out.println(n);
        numbers.forEach(integerConsumer);

        System.out.println();

        numbers.forEach(System.out::println);

    }
}
