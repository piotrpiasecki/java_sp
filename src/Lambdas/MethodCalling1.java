package Lambdas;

import java.util.function.IntSupplier;
import java.util.function.ToIntFunction;

public class MethodCalling1
{
    public static void main(String[] args)
    {
        Object object = new Object();
        IntSupplier equalsMethodOnObject = object::hashCode;
        System.out.println(equalsMethodOnObject.getAsInt());

        System.out.println(object.hashCode());

        ToIntFunction<Object> hashCodeMethodOnClass = Object::hashCode;
        Object objectInstance = new Object();
        System.out.println(hashCodeMethodOnClass.applyAsInt(objectInstance));


    }
}
