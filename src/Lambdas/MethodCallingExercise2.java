package Lambdas;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class MethodCallingExercise2
{
    public static void main(String[] args)
    {
        Object objectInstance = new Object();
        Consumer<Object> methodCalling = objectInstance::equals;

        Supplier<Human> humanSupplier = Human::new;

        Human human = humanSupplier.get();

/*        Human human2 = (humanSupplier.get() h ->
        {
            h.setAge(27);
            h.setame("Piotr");
        };)*/
    }




}
