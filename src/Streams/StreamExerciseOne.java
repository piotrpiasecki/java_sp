package Streams;

import java.util.Comparator;
import java.util.stream.Stream;

public class StreamExerciseOne
{

    public static void main(String[] args)
    {
        double highestRanking = 0;
        BoardGame bestGame = null;
        for (BoardGame game : BoardGameChoice.boardGameList)
        {
            if (game.name.contains("a"))
            {
                if (game.rating > highestRanking)
                {
                    highestRanking = game.rating;
                    bestGame = game;
                }
            }
        }
        System.out.println(bestGame.name);

        Stream<BoardGame> boardGameStream = BoardGameChoice.boardGameList.stream();

        boardGameStream
                .filter(g -> g.name.contains("a"))
                .sorted(Comparator.comparing(BoardGame::getRating).reversed())
                .limit(1)
                .forEach(g -> System.out.println(g.name));


    }

}
