package Streams;

import java.util.stream.IntStream;

public class StreamExercise
{
    public static void main(String[] args)
    {
        IntStream intStream = IntStream.range(0,8);
        System.out.println("Before");

        intStream = intStream.filter(n -> (n % 2 == 0));
        System.out.println("During 1");

        intStream = intStream.map(n ->
        {
            System.out.println("> " + n);
            return n;
        });
        System.out.println("During 2");

        intStream = intStream.limit(2);
        System.out.println("During 3");

        intStream.forEach(System.out::println);
        System.out.println("After");
    }

}
