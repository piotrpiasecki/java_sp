package Streams;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class MinWithReduce
{
    public static void main(String[] args)
    {
        Collection<String> names = Arrays.asList("Marcin", "Marek", "Mariusz", "Marian");
        Optional<String> name = min(names, Comparator.comparingInt(String::length));
        System.out.println(name.get());

        String[] namesArray = new String[]{"Marcin", "Marek", "Mariusz", "Marian"};

        System.out.println(
                Arrays.stream(namesArray)
                .reduce(BinaryOperator
                        .minBy(Comparator
                                .comparingInt(String::length))).get()
        );

    }

    public static <T> Optional<T> min(Collection<T> collection, Comparator<T> comparator)
    {
        return collection.stream()
                .reduce(BinaryOperator.minBy(comparator));
    }


}
