package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NamedGroups
{
    public static void main(String[] args)
    {
        Pattern pattern = Pattern.compile("(?<day>\\d{2})\\.(?<month>\\d{2})\\.(?<year>\\d{4})");

        String sequence = "09.02.1992";

        Matcher matcher = pattern.matcher(sequence);

        matcher.matches();

        System.out.println(matcher.group("day"));
        System.out.println(matcher.group("month"));
        System.out.println(matcher.group("year"));
    }
}
