package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise1
{
    public static void main(String[] args)
    {
        Pattern compiledPattern = Pattern.compile("Piotr");
        Matcher matcher = compiledPattern.matcher("Nazywam się Piotr Piasecki");

        System.out.println(matcher.find());
        System.out.println(matcher.matches());
        System.out.println(compiledPattern.matcher("Nazywam się Piotr Piasecki").find());
        System.out.println(compiledPattern.matcher("Nazywam się Piotr Piasecki").matches());
    }
}
