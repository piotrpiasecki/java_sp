package Regex;


import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

public class Exercise2
{
    @Test
    void testSymbolPlus()
    {
        Pattern pattern = Pattern.compile("trampo+lina");
        assertTrue(pattern.matcher("trampolina").matches());
        assertTrue(pattern.matcher("trampoooolina").matches());
        assertFalse(pattern.matcher("tramplina").matches());
    }

    @Test
    void testClasswithMultipleRanges()
    {
        Pattern pattern = Pattern.compile("[a-cA-C0-3]bum");
        assertTrue(pattern.matcher("abum").matches());
    }

    @Test
    public void testBasicGroups()
    {
        Pattern pattern = Pattern.compile("[^-]*--(\\w+)--.*");
        Matcher matcher = pattern.matcher("Ala ma kota. Kota ma na imie --Zygmunt--. Kot jest czarny. --kutafon--");
        matcher.matches();
        assertEquals("Zygmunt", matcher.group(1));
    }
}
