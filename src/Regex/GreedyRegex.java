package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GreedyRegex
{
    public static void main(String[] args)
    {
        Pattern pattern0 = Pattern.compile("<(.+)>");
        Pattern pattern1 = Pattern.compile("<([^>]+)>");
        Pattern pattern2 = Pattern.compile("<(.+?)>");
        Pattern pattern3 = Pattern.compile("<(.*?)>");

        Pattern chosenPattern = pattern3;

        Matcher matcher = chosenPattern.matcher("<em>some emphasized text</em>");

        System.out.println(matcher.find());
        System.out.println(matcher.group(1));
    }
}
