package Regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class RegexTask2
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        Pattern number = Pattern.compile("[1-9]\\d*[A-Z]?\\\\[1-9]\\d*[A-Z]?");

        System.out.println("Podaj numer domu i mieszkania (jako separatora użyj znaku \"\\\"): ");

        System.out.printf("Adres lokalu %s poprawny",
                (number.matcher(input.nextLine()).matches() ? "jest" : "nie jest"));

    }
}
