package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Alternative
{
    public static void main(String[] args)
    {
        Pattern pattern = Pattern.compile("pies|kot|lew");

        String sequence = "kot";

        Matcher matcher = pattern.matcher(sequence);

        System.out.println(matcher.find());
        System.out.println(matcher.matches());
        System.out.println(matcher.group(0));

    }
}
