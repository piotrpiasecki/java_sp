package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NoCapturingGroup
{
    public static void main(String[] args)
    {
        Pattern pattern = Pattern.compile("(Ala|Ola) ma (kota|psa)");
        Pattern pattern1 = Pattern.compile("(?:Ala|Ola) ma (kota|psa)");

        Matcher matcher = pattern.matcher("Ola ma psa");
        Matcher matcher1 = pattern1.matcher("Ola ma psa");

        System.out.println(matcher.matches());
        System.out.println(matcher.group(0));
        System.out.println(matcher.group(1));
        System.out.println(matcher.group(2));

        System.out.println();

        System.out.println(matcher1.matches());
        System.out.println(matcher1.group(0));
        System.out.println(matcher1.group(1));
        System.out.println(matcher1.group(2));
    }
}
