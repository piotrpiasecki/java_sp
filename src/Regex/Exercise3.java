package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise3
{
    public static void main(String[] args)
    {
        Pattern pattern = Pattern.compile("[^-]*--(\\w+)--.*");
        Matcher matcher = pattern.matcher("Ala ma kota. Kota ma na imie --Zygmunt--. Kot jest czarny.");
        System.out.println(matcher.matches());
        System.out.println(matcher.group(1));
    }
}
