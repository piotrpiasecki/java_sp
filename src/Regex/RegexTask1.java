package Regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTask1
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        Pattern number = Pattern.compile("-?\\d+(,\\d+)?");

        System.out.println("Podaj liczbę zmiennoprzecinkową (jako separatora dziesiętnego użyj przecinka: ");

        System.out.printf("Liczba %s liczbą zmiennoprzecinkową",
                (number.matcher(input.nextLine()).matches() ? "jest" : "nie jest"));

    }
}
