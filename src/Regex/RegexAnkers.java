package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

public class RegexAnkers
{
    public static void main(String[] args)
    {
        Pattern pattern1 = Pattern.compile("\\d+");
        Matcher matcher1 = pattern1.matcher("abc123def");
        Pattern pattern2 = Pattern.compile("^\\d+$");
        Matcher matcher2 = pattern2.matcher("abc123def");


        System.out.println(matcher1.find());
        System.out.println(matcher1.matches());
        System.out.println(matcher2.find());
        System.out.println(matcher2.matches());


    }
}
