package Weight;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface WeightUnit
{
    int SCALE = 4;
    RoundingMode ROUNDING_MODE = RoundingMode.CEILING;

    BigDecimal POUND_TO_KILOGRAM_RATIO = new BigDecimal("0.453592").setScale(SCALE, ROUNDING_MODE);
}
