package Weight;

import java.math.BigDecimal;

public class Kilogram implements WeightUnit
{
    public final BigDecimal value;

    public Kilogram(BigDecimal value)
    {
        if (BigDecimal.ZERO.compareTo(value) > 0)
        {
            throw new IllegalArgumentException("Weight must be non-negative!");
        }

        this.value = value;
    }

    public Pound toPounds()
    {
        return new Pound(value.divide(POUND_TO_KILOGRAM_RATIO, SCALE, ROUNDING_MODE));
    }
}