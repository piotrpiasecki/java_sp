package StringCache;

public class CacheTest
{

    public static void main(String[] args)
    {
        String variable_2 = "some variable";
        String variable_1 = "some variable";

        System.out.println(variable_1.equals(variable_2));
        System.out.println(variable_1 == variable_2);

        System.out.println();

        String variable_3 = new String("some variable");
        String variable_4 = new String("some variable");

        System.out.println(variable_3.equals(variable_4));
        System.out.println(variable_3 == variable_4);

        System.out.println();

        String variable_5 = "some variable";
        String variable_6 = new String("some variable").intern();

        System.out.println(variable_5.equals(variable_6));
        System.out.println(variable_5 == variable_6);

    }
}
