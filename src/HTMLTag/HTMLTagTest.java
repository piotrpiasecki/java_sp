package HTMLTag;

public class HTMLTagTest
{
    public static void main(String[] args) throws Exception
    {
        try (
                HTMLTag h1 = new HTMLTag("h1");
                HTMLTag em = new HTMLTag("em")
            )
        {
            em.body("My text");
        }
    }
}
