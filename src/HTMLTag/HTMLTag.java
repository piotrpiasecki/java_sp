package HTMLTag;

public class HTMLTag implements AutoCloseable
{
    private String marker;

    public HTMLTag(){}

    public HTMLTag(String marker)
    {
        this.marker = marker;
        System.out.println("<" + marker + ">");
    }

    public void body(String text)
    {
        System.out.println(text);
    }

    @Override
    public void close() throws Exception
    {
        System.out.println("</" + marker + ">");
    }
}
