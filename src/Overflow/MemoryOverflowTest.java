package Overflow;

public class MemoryOverflowTest
{
    public static final int X = Integer.MAX_VALUE * 9 / 10;
    public static Long[][] table = new Long[X][X];

    public static void main(String[] args)
    {
        for (int i1 = 1; i1 < X; i1++)
        {
            for (int i2 = 1; i2 < X; i2++)
            {
                table[i1][i2] = Long.MAX_VALUE;
            }
        }
    }
}