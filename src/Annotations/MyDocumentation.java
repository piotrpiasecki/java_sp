package Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyDocumentation
{
    String author() default "Piotr Piasecki";
    String comment() default "No comments";
}
