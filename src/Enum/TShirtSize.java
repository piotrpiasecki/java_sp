package Enum;

public enum TShirtSize
{
    S,
    M,
    L,
    XL,
    XXL
}
