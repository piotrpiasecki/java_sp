package Enum;

public enum Formatter
{
    CALM
            {
                public String format(String message)
                {
                    return "Here is Your message: " + message;
                }
            },
    NERVOUS
            {
                public String format(String message)
                {
                    return "HERE'S YOUR F*CKING MESSAGE!: " + message;
                }
            };

            public abstract String format(String message);
}
