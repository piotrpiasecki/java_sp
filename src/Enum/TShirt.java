package Enum;

public class TShirt
{
    private String manufacturer;
    private TShirtSize size;

    public TShirt(String manufacturer, TShirtSize size)
    {
        this.manufacturer = manufacturer;
        this.size = size;
    }

    public static void main(String[] args)
    {
        TShirt tShirt = new TShirt("Lee", TShirtSize.M);
        String monit = Formatter.CALM.format("Zakupiłeś nową koszulkę w rozmiarze: " + tShirt.size.toString());
        System.out.println(monit);
    }
}
