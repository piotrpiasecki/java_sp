package Enum;

public enum Computation
{
    MULTIPLY
            {
                public double perform(double x, double y)
                {
                    double result = x * y;
                    return result;
                }
            },
    DIVIDE
            {
                public double perform(double x, double y)
                {
                    double result = x / y;
                    return result;
                }
            },
    ADD
            {
                public double perform(double x, double y)
                {
                    double result = x + y;
                    return result;
                }
            },
    SUBTRACT
            {
                public double perform(double x, double y)
                {
                    double result = x - y;
                    return result;
                }
            };

    public abstract double perform(double x, double y);
}
