package Enum;

import java.util.ArrayList;
import java.util.List;

public class ComputationTest
{
    public static void main(String[] args)
    {
        double x = 2;
        double y = -3;
//        double[] results = new double[4];
        List<Double> resultsList = new ArrayList<>();
        resultsList.add(Computation.ADD.perform(x, y));
        resultsList.add(Computation.SUBTRACT.perform(x, y));
        resultsList.add(Computation.MULTIPLY.perform(x, y));
        resultsList.add(Computation.DIVIDE.perform(x, y));

        for (double result : resultsList)
        {
            System.out.printf("Result: %.2f" + System.lineSeparator(), result);
        }
    }
}
