package LinkedLists;

import GenericClasses.Pair;

public class DoubleLinkedList<E>
{
    private Node<E> first;
    private Node<E> last;

    private static class Node<E>
    {
        private E element;
        private Node<E> previous;
        private Node<E> next;

        public Node(E element)
        {
            this.element = element;
        }
    }

    public boolean isEmpty()
    {
        return this.first == null;
    }

    public int size()
    {
        int size = 0;
        Node<E> currentNode = this.first;

        while (currentNode != null)
        {
            currentNode = currentNode.next;
            size++;
        }

        return size;
    }

    public E get(int index)
    {
        return getNode(index).element;
    }

    public Node<E> getNode(int index)
    {
        if (isEmpty() || index < 0)
        {
            throw new IndexOutOfBoundsException("Index " + index);
        }

        Node<E> currentNode = first;
        int currentIndex = index;

        while (currentIndex > 0)
        {
            if (currentNode == null)
            {
                throw new IndexOutOfBoundsException("Index " + index);
            }

            currentNode = currentNode.next;
            currentIndex--;
        }

        return currentNode;
    }

    public boolean add(int index, E element)
    {
        if (index < 0)
        {
            throw new IndexOutOfBoundsException("Index " + index);
        }

        if (this.first == null && index == 0)
        {
            this.first = new Node<E>(element);
            this.last = first;
            return true;
        }

        Node<E> nodeAtIndex = getNode(index);

        if (nodeAtIndex == null)
        {
            Node<E> previousLast = this.last;
            this.last = new Node<E>(element);
            last.previous = previousLast;
            previousLast.next = last;
            return true;
        }

        if (nodeAtIndex.previous == null)
        {
            Node<E> previousFirst = first;
            first = new Node<>(element);
            first.next = previousFirst;
            previousFirst.previous = first;
            return true;
        }

        Node<E> newNode = new Node<>(element);
        Node<E> previous = nodeAtIndex.previous;
        previous.next = newNode;
        newNode.previous = previous;
        newNode.next = nodeAtIndex;
        nodeAtIndex.previous = newNode;

        return true;
    }

    public E remove(int index)
    {
        Node<E> nodeToRemove = getNode(index);
        Node<E> previousNode = nodeToRemove.previous;
        Node<E> nextNode = nodeToRemove.next;
        E removedElement = nodeToRemove.element;

        if (previousNode == null)
        {
            if (nextNode == null)
            {
                this.first = null;
                this.last = null;
            }
            else
            {
                this.first = nextNode;
                nextNode.previous = null;
            }

            return removedElement;
        }

        if (nextNode == null)
        {
            this.last = previousNode;
            previousNode.next = null;

            return removedElement;
        }

        previousNode.next = nextNode;
        nextNode.previous = previousNode;

        return removedElement;
    }
}
