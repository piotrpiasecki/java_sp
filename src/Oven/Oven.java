package Oven;

import java.util.Objects;

public class Oven implements BakingOven, RoastingOven
{
    private int time;
    private int temperature;

    @Override
    public void bakeCookies()
    {
        time = 120;
        temperature = 200;
        turnOn();
    }

    @Override
    public void bakeBread()
    {
        time = 90;
        temperature = 180;
        turnOn();
    }

    @Override
    public void roastChicken()
    {
        time = 130;
        temperature = 300;
        turnOn();
    }

    private void turnOn()
    {
        System.out.printf("%nStart. Heat up to %d and work for %d minutes", temperature, time);
    }


    public static void main(String[] args)
    {
        Oven oven = new Oven();

        oven.bakeBread();
        oven.bakeCookies();
        oven.roastChicken();

        BakingOven bakingOven = oven;
        RoastingOven roastingOven = oven;

        bakingOven.bakeBread();
        bakingOven.bakeCookies();

        roastingOven.roastChicken();
    }
}
