package Garfield;

public class MainApp
{
    public static void main(String[] args)
    {
        Garfield garfield = new Garfield();
        FatCat fatCat = new Garfield();
        LasagnaEater lasagnaEater = new Garfield();
        Cat cat = new Garfield();


        garfield.getLasagnaRecipe();
        garfield.getName();
        garfield.getWeight();

        fatCat.getLasagnaRecipe();
        fatCat.getName();
        fatCat.getWeight();

        lasagnaEater.getLasagnaRecipe();

        cat.getName();


    }
}
