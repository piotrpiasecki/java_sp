package InnerClasses;

public class GreetingModuleTest
{
    public void someMethod()
    {
        GreetingModule greetingModule = new GreetingModule()
        {
            @Override
            public void sayHello()
            {
                System.out.println("Hello!");
            }
        };
    }
}
