package InnerClasses;

import java.util.Arrays;

public class LocalClassTest
{
    public static void localClassInstantiation(String[] args)
    {
        class LocalClass
        {
            @Override
            public String toString()
            {
                return "Method's arguments: " + Arrays.toString(args);
            }
        }

        LocalClass localClassInstance = new LocalClass();
        System.out.println(localClassInstance);
    }
}
