package InnerClasses;

import java.util.*;

public class Exercise
{
    public static void main(String[] args)
    {
        List<String> wordList = new ArrayList<>();
        Scanner input = new Scanner(System.in);

        for (int i = 1; i <= 5; i++)
        {
            System.out.println("Type in a word");
            wordList.add(input.nextLine());
        }

        for (String element : wordList)
        {
            System.out.print(element + " ");
        }

        System.out.println();

        Collections.sort(wordList, new Comparator<String>()
                {
                    @Override
                    public int compare(String o1, String o2)
                    {
                        int result = -1;

                        if (o2.length() < o1.length())
                        {
                            result = 1;
                        }

                        return result;
                    }
                }
        );

        for (String element : wordList)
        {
            System.out.print(element + " ");
        }
    }
}
