package InnerClasses;

public class OuterClass
{
    public class InnerClass{}

    public static class InnerClass2{}

    public InnerClass instantiate()
    {
        return new InnerClass();
    }

    public InnerClass2 instantiate2()
    {
        return new InnerClass2();
    }
}
