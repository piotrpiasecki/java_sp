package InnerClasses;

public class AnonymousClasses
{
    public static class Robot
    {
        private final GreetingModule greetingModule;

        public Robot(GreetingModule greetingModule)
        {
            this.greetingModule = greetingModule;
        }

        public void saySomething()
        {
            greetingModule.sayHello();
        }

        public static void main(String[] args)
        {
            Robot jan = new Robot(new GreetingModule()
            {
                @Override
                public void sayHello()
                {
                    System.out.println("Cześć!");
                }
            });

            Robot john = new Robot(new GreetingModule()
            {
                @Override
                public void sayHello()
                {
                    System.out.println("Hello!");
                }
            });

            jan.saySomething();
            john.saySomething();
        }
    }
}
