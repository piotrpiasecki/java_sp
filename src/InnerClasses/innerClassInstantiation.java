package InnerClasses;

public class innerClassInstantiation
{
    public static void main(String[] args)
    {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClassInstantion1 = outerClass.instantiate();
        OuterClass.InnerClass innerClassInstantion2 = outerClass.new InnerClass();

        OuterClass.InnerClass2 innerClass2Instantion1 = outerClass.instantiate2();
        OuterClass.InnerClass2 innerClass2Instantion2 = new OuterClass.InnerClass2();

        String[] arguments = {"\nargument1", "\nargument2\n"};

        LocalClassTest.localClassInstantiation(arguments);
    }
}
