package JSON;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;
import java.util.Collections;

public class JSONStream
{
    public static void main(String[] args)
    {
        JsonGeneratorFactory generatorFactory = Json.createGeneratorFactory(Collections.singletonMap(JsonGenerator.PRETTY_PRINTING, true));
        JsonGenerator generator = generatorFactory.createGenerator(System.out);
        generator
                .writeStartObject()
                .write("imię", "Marcin")
                .write("nazwisko", "Pietraszek")
                .writeStartObject("strona")
                .write("adres www", "https://www.samouczekprogramisty.pl")
                .writeStartArray("artykuły")
                .writeStartObject()
                .write("tytuł", "Format JSON w języku Java")
                .writeStartObject("data publikacji")
                .write("rok", 2018)
                .write("miesiąc", 9)
                .write("dzień", 13)
                .writeEnd()
                .writeEnd()
                .writeEnd()
                .writeEnd()
                .writeEnd().flush();

        JsonParserFactory parserFactory = Json.createParserFactory(Collections.emptyMap());
//        JsonParser parser = parserFactory.createParser(buildObject());
        JsonParser parser = null;

        while (parser.hasNext())
        {
            JsonParser.Event event = parser.next();
            switch (event)
            {
                case START_OBJECT:
                    System.out.println("{");
                    break;
                case END_OBJECT:
                    System.out.println("}");
                    break;
                case START_ARRAY:
                    System.out.println("[");
                    break;
                case END_ARRAY:
                    System.out.println("]");
                    break;
                case KEY_NAME:
                    System.out.print(String.format("\"%s\": ", parser.getString()));
                    break;
                case VALUE_NUMBER:
                    System.out.println(parser.getBigDecimal());
                    break;
                case VALUE_STRING:
                    System.out.println(String.format("\"%s\"", parser.getString()));
                    break;
                default:
                    System.out.println("true, false or null");
            }
        }
    }
}
