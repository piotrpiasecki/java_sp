package JSON;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.time.LocalDate;

public class JSONBExercise
{
    public static void main(String[] args)
    {
        Newspaper newspaper = new Newspaper(
                "Samouczek Programisty", 100_000, LocalDate.of(2018, 9, 13));
        Jsonb jsonb = JsonbBuilder.create();
        String newspaperRepresentation = jsonb.toJson(newspaper);

        System.out.println(newspaperRepresentation);

        Newspaper copyOfANewspaper = jsonb.fromJson(newspaperRepresentation, Newspaper.class);
        System.out.println(copyOfANewspaper.getIssueDate());
        System.out.println(copyOfANewspaper);
    }


}
