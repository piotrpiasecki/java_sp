package JSON;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import javax.json.*;
import javax.json.stream.JsonGenerator;

public class JSONExercise
{
    public static void main(String[] args)
    {
        JsonBuilderFactory builderFactory = Json.createBuilderFactory(Collections.emptyMap());
        JsonObject publicationDataObject = builderFactory.createObjectBuilder()
                .add("year", 2018)
                .add("month", 9)
                .add("day", 18)
                .build();

        JsonObject articleObject = builderFactory.createObjectBuilder()
                .add("title", "JSON format in Java")
                .add("publication date", publicationDataObject)
                .build();

        JsonArray articlesArray = builderFactory.createArrayBuilder().add(articleObject).build();

        JsonObject webPageObject = builderFactory.createObjectBuilder()
                .add("www address", "https://www.samouczekprogramisty.pl")
                .add("articles", articlesArray)
                .build();

        JsonObject authorObject = builderFactory.createObjectBuilder()
                .add("name", "Piotr")
                .add("surname", "Piasecki")
                .add("webpage", webPageObject)
                .build();

        System.out.println(authorObject.toString());

        System.out.println();

        Map<String, ?> config = Collections.singletonMap(JsonGenerator.PRETTY_PRINTING,  true);
//        Map<String, Boolean> config = Collections.singletonMap(JsonGenerator.PRETTY_PRINTING,  true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(config);
        writerFactory.createWriter(System.out).write(authorObject);

        String jsonObject = authorObject.toString();
        InputStream inputStream = new ByteArrayInputStream(jsonObject.getBytes());

        JsonReaderFactory readerFactory = Json.createReaderFactory(Collections.emptyMap());
//        JsonReader jsonReader = readerFactory.createReader(inputStream);

//        try (JsonReader jsonReader = readerFactory.createReader(new ByteArrayInputStream(jsonDocument.getBytes())))
//        {
//            JsonObject jsonObject = jsonReader.readObject();
//            System.out.println(jsonObject
//                    .getJsonObject("webpage")
//                    .getJsonArray("articles")
//                    .get(0).asJsonObject()
//                    .getJsonObject("publication date")
//            );
//
//            JsonReaderFactory readerFactory = Json.createReaderFactory(Collections.emptyMap());
//            try (JsonReader jsonReader = readerFactory.createReader(new ByteArrayInputStream(jsonDocument.getBytes()))) {
//                JsonStructure jsonStructure = jsonReader.read();
//                System.out.println(jsonStructure.getValue("/strona/artykuły/0/data publikacji"));
//            }
//        }
    }
}
