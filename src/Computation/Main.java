package Computation;

import java.util.Scanner;

public class Main
{
    Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {
        Main main = new Main();
        Computation computation;

        if (main.shouldMultiply())
        {
            computation = new Multiplication(); // zaimplementuj brakującą klasę
        } else
        {
            computation = new Addition(); // zaimplementuj brakującą klasę
        }

        double argument1 = main.getArgument();
        double argument2 = main.getArgument();

        double result = computation.compute(argument1, argument2);
        System.out.println("Wynik: " + result);
    }

    private boolean shouldMultiply()
    {
        System.out.println("Jeżli chcesz mnożyć, wpisz \"true\", w przeciwnym przypadku wciśnij \"false\". " +
                "Potwierdź wciskając klawisz Enter.");

        boolean decision = input.nextBoolean();

        return decision;
    }

    private double getArgument()
    {
        System.out.println("Podaj składnik/czynnik.");

        double argument = input.nextDouble();

        return argument;
    }
}