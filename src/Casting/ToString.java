package Casting;

public class ToString
{
    public static void main(String[] args)
    {
        String x = "123" + new Object();
        String y = new Object() + "123";
        String z = 1 + "123";

        System.out.printf("%s%n%s%n%s", x, y, z);
    }
}
