package GenericClasses;

public class StandardBox<T>
{
    private T object;

    public StandardBox(T object)
    {
        this.object = object;
    }

    public T getObject()
    {
        return object;
    }
}
