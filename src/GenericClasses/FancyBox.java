package GenericClasses;

public class FancyBox<T> extends StandardBox<T>
{
    public FancyBox(T object)
    {
        super(object);
    }

    public void saySomethingFancy()
    {
        System.out.println("The " + getObject().toString() + " is cool!");
    }

    public static void method1(FancyBox<?> box) {
        Object object = box.getObject();
        System.out.println(object);
    }

    public static void plainWildcard() {
        method1(new FancyBox<>(new Object()));
        method1(new FancyBox<>(new Square()));
        method1(new FancyBox<>(new Apple()));
    }


}
