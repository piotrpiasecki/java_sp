package GenericClasses;

public class Main
{
    public static void main(String[] args)
    {
        BoxOnSteroids<Apple> appleBox = new BoxOnSteroids<>(new Apple());
        BoxOnSteroids<Orange> orangeBox = new BoxOnSteroids<>(new Orange());

        Pair<BoxOnSteroids<Apple>, BoxOnSteroids<Orange>> pairOfBoxes =
                new Pair<>(
                        new BoxOnSteroids<>(new Apple()),
                        new BoxOnSteroids<>(new Orange())
                );

        BoxOfFigures circleBox = new BoxOfFigures<>(new Circle());

        Rectangle rectangle = new Square();
        BoxOfFigures<Rectangle> rectangleBox = new BoxOfFigures<>(new Square());

        StandardBox<String> justARegularBox = new FancyBox<>("O B J E C T");
        FancyBox<String> anotherBox = (FancyBox<String>) justARegularBox;
        anotherBox.saySomethingFancy();

        FancyBox.plainWildcard();

    }
}
