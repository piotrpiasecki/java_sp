package GenericClasses;

public class Circle implements Figure
{
    @Override
    public String getName()
    {
        return "circle";
    }
}
