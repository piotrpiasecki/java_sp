package GenericClasses;

public interface Figure
{
    String getName();
}
