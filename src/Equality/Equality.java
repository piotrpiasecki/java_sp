package Equality;

import java.util.Objects;

public class Equality
{
    public class Human
    {
        private String name;
        private String surname;
        private String pesel;

        public Human(String name, String surname, String pesel)
        {
            this.name = name;
            this.surname = surname;
            if (pesel == null)
            {
                throw new IllegalArgumentException("Everyone needs to have PESEL");
            }
            this.pesel = pesel;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getSurname()
        {
            return surname;
        }

        public void setSurname(String surname)
        {
            this.surname = surname;
        }

        public String getPesel()
        {
            return pesel;
        }

        public void setPesel(String pesel)
        {
            this.pesel = pesel;
        }

        @Override
        public boolean equals(Object o)
        {
/*            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Human human = (Human) o;
            return Objects.equals(pesel, human.pesel);*/

            if (o == null)
            {
                return false;
            }

            if (!(o instanceof Human))
            {
                return false;
            }

            Human human = (Human) o;
            return pesel.equals(human.getPesel());
        }

        @Override
        public int hashCode()
        {
//            return Objects.hash(pesel);
            return 7 * pesel.hashCode();
        }

    }
}
