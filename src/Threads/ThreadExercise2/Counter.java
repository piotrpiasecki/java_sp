package Threads.ThreadExercise2;

public class Counter
{
    private int count = 0;

    synchronized void increment()
    {
        count += 1;
        notify();
    }

    synchronized void waitUntil(int desiredCount)
    {
        try
        {
            while (desiredCount > count)
            {
                wait();
            }
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }
}
