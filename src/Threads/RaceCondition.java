package Threads;

public class RaceCondition
{
    public static void main(String[] args) throws InterruptedException
    {
        Counter c = new Counter();

        Runnable r = () ->
        {
            for (int i = 1; i <= 100_000; i++)
            {
                c.increment();
            }
        };

        Thread thread_1 = new Thread(r);
        Thread thread_2 = new Thread(r);
        Thread thread_3 = new Thread(r);

        thread_1.start();
        thread_2.start();
        thread_3.start();

        thread_1.join();
        thread_2.join();
        thread_3.join();

        System.out.println(c.getValue());
    }
}
