package Threads;

public class ThreadExercise
{
    public static void main(String[] args)
    {
        System.out.println(getThreadName() + ": Start");

        Thread thread = new Thread(
                () ->
                {
                    System.out.println(getThreadName() + ": Start");
                    for (int i = 1; i <= 5; i++)
                    {
                        System.out.println(getThreadName() + ": Step " + i);
                    }
                    System.out.println(getThreadName() + ": Stop");
                }
        );

        thread.start();

        for (int j = 1; j <= 5; j++)
        {
            System.out.println(getThreadName() + ": Step " + j);
        }

        System.out.println(getThreadName() + ": Stop");
    }

    public static String getThreadName()
    {
        return Thread.currentThread().getName();
    }
}
