package Threads;

import java.util.LinkedList;
import java.util.Queue;

public class Counter
{
    private int value;

    public void increment()
    {
        synchronized (this)
        {
            value += 1;
        }
    }

    public int getValue()
    {
        return value;
    }

    public synchronized static void sampleStaticMethod()
    {
        System.out.println("x");
    }

    public static void anotherSampleStaticMethod()
    {
        synchronized (Counter.class)
        {
            System.out.println("xxx");
        }
    }
    private static final Queue<String> queue = new LinkedList<>();
}
