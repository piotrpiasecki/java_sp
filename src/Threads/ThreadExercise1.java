package Threads;

import GenericClasses.Pair;

import java.util.*;

public class ThreadExercise1
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj liczbę wątków: ");
        int noOfThreads = input.nextInt();

        Map<Integer, Thread> threadMap = generateThreadMap(noOfThreads);

        //startNestedThreads(threadMap);
        startReversedNestedThreads(threadMap);

    }

    public synchronized static void startNestedThreads(Map<Integer, Thread> threadMap)
    {
        for (int i = 1; i <= threadMap.size(); i++)
        {
            threadMap.get(i).start();
        }
    }

    public synchronized static void startReversedNestedThreads(Map<Integer, Thread > threadMap)
    {
        for (int i = threadMap.size(); i >= 1; i--)
        {
            threadMap.get(i).start();
        }
    }

    public synchronized static Map<Integer, Thread> generateThreadMap(int noOfThreads)
    {
        Map<Integer, Thread> threadMap = new HashMap<>();

        for (int i = 1; i <= noOfThreads; i++)
        {
            Thread newThread = new Thread(() -> {
                System.out.println(Thread.currentThread().getName());
            });
            threadMap.put(i, newThread);
        }
        return threadMap;
    }
}
