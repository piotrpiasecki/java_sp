package Files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileExercise1
{
    public static void main(String[] args) throws IOException
    {
        String filePath = "C:\\Users\\piase\\Desktop\\Programowanie\\java_SP\\SP_Exercises\\example_file.txt";
        int number = 1234567;
        FileWriter fileWriter = null;

        try
        {
            fileWriter = new FileWriter(filePath);
            fileWriter.write(Integer.toString(number));
        }
        finally
        {
            if (fileWriter != null)
            {
                fileWriter.close();
            }
        }

        int numberRead = 0;
        String numberAsString = null;
        BufferedReader bufferedReader = null;

        try
        {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            numberAsString = bufferedReader.readLine();
        }
        finally
        {
            if (bufferedReader != null)
            {
                bufferedReader.close();
            }
        }

        numberRead = Integer.parseInt(numberAsString);

        System.out.println(numberAsString);
        System.out.println(numberRead);

    }
}
