package Files;

import java.io.*;

public class BinaryFileExercise1
{
    public static void main(String[] args) throws IOException
    {
        String filePath = "C:\\Users\\piase\\Desktop\\Programowanie\\java_SP\\SP_Exercises\\example_binary_file.txt";
        int number = 1234567;
        DataOutputStream dataOutputStream = null;

        try
        {
            dataOutputStream = new DataOutputStream(new FileOutputStream(filePath));
            dataOutputStream.writeInt(number);
        }
        finally
        {
            if (dataOutputStream != null)
            {
                dataOutputStream.close();
            }
        }

        int numberRead = 0;
        String numberAsString = null;
        DataInputStream dataInputStream = null;

        try
        {
            dataInputStream = new DataInputStream(new FileInputStream(filePath));
            numberAsString = dataInputStream.readLine();
        }
        finally
        {
            if (dataInputStream != null)
            {
                dataInputStream.close();
            }
        }

        numberRead = Integer.parseInt(numberAsString);

        System.out.println(numberAsString);
        System.out.println(numberRead);

    }
}
