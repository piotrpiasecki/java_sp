package Files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileWriterExercise
{
    public static void main(String[] args) throws IOException
    {
        Scanner input = new Scanner(System.in);
        String filePath = "C:\\Users\\piase\\Desktop\\Programowanie\\java_SP\\SP_Exercises\\exercise_file.txt";
        String toWrite = null;
        BufferedWriter bufferedWriter = null;

        System.out.println("Wprowadź tekst. Gdy będziesz chciał skończyc wprowadź \"-\" w nowej linii.");

        try
        {
            bufferedWriter = new BufferedWriter(new FileWriter(filePath));
            toWrite = input.nextLine();

            while (!toWrite.equals("-"))
            {
                bufferedWriter.write(toWrite + System.lineSeparator());
                toWrite = input.nextLine();
            }
        }
        finally
        {
            if (bufferedWriter != null)
            {
                bufferedWriter.close();
            }
        }
    }
}
