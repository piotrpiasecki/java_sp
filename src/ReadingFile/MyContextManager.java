package ReadingFile;

public class MyContextManager implements AutoCloseable
{

    public MyContextManager()
    {
        System.out.println("Creating MyContextManager object.");
    }

    public void doSomething()
    {
        System.out.println("Inside doSomething method.");
    }

    @Override
    public void close() throws Exception
    {
        System.out.println("Inside close method.");
    }
}
