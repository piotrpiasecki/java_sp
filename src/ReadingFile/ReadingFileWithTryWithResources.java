package ReadingFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadingFileWithTryWithResources
{
    public static void main(String[] args)
    {
        String inputPath = null;

        try (BufferedReader fileReader = new BufferedReader(new FileReader(inputPath)))
        {
            fileReader.readLine();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
