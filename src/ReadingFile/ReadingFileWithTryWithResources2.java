package ReadingFile;

import javax.annotation.processing.Filer;
import java.io.*;

public class ReadingFileWithTryWithResources2
{
    public static void main(String[] args)
    {
        String inputPath = null;
        String outputPath = null;

        try (
                BufferedReader fileReader = new BufferedReader(new FileReader(inputPath));
                BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputPath));
                )
        {
            String line = fileReader.readLine();
            fileWriter.write(line);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
