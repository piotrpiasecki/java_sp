package ReadingFile;

public class MyContextManagerTest
{
    public static void main(String[] args) throws Exception
    {
        try (MyContextManager myContextManager = new MyContextManager())
        {
            myContextManager.doSomething();
        }
    }
}
