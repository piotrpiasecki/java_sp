package Maps;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SimpleHashMap<K, V>
{
    public static final float LOAD_FACTOR = 0.75F;
    private static final int INITIAL_CAPACITY = 4;

    private int size;
    private int threshold;

    private List<Entry<K, V>>[] table;

    public SimpleHashMap()
    {
//        this.table = (List<Entry<K, V>>[]) new Object[INITIAL_CAPACITY];
        this.table = new List[INITIAL_CAPACITY];

        threshold = (int) (INITIAL_CAPACITY * LOAD_FACTOR);
    }

    private int hash(K key)
    {
        if (key == null)
        {
            return 0;
        }
        return Math.abs(key.hashCode() % table.length);
    }

    private void resize()
    {
        if (table.length == Integer.MAX_VALUE)
        {
            return;
        }

        List<Entry<K, V>>[] oldTable = table;
        table = new List[table.length * 2];
        threshold = (int) (table.length * LOAD_FACTOR);

        for (List<Entry<K, V>> bucket : oldTable)
        {
            if (bucket == null)
            {
                continue;
            }

            for (Entry<K, V> entry : bucket)
            {
                int hash = hash(entry.getKey());

                if (table[hash] == null)
                {
                    table[hash] = new LinkedList<>();
                }

                List<Entry<K, V>> newBucket = table[hash];
                newBucket.add(entry);
            }
        }
    }

    private boolean keysEqual(K key1, K key2)
    {
        return key1.equals(key2);
    }

    public V put(K key, V value)
    {
        int hash = hash(key);

        if (table[hash] == null)
        {
            table[hash] = new LinkedList<>();
        }

        V oldValue = null;
        boolean keyExists = false;
        List<Entry<K, V>> bucket = table[hash];

        for (Entry<K, V> entry : bucket)
        {
            if (keysEqual(key, entry.getKey()))
            {
                oldValue = entry.getValue();
                entry.setValue(value);
                keyExists = true;
                break;
            }
        }

        if (!keyExists)
        {
            bucket.add(new Entry<K, V>(key, value));
            size++;
        }

        if (size == threshold)
        {
            this.resize();
        }

        return oldValue;
    }

    public V get(K key)
    {
        int hash = hash(key);
        List<Entry<K, V>> bucket = table[hash];

        if (bucket != null)
        {
            for (Entry<K, V> entry : bucket)
            {
                if (keysEqual(key, entry.getKey()))
                {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    public V remove(K key)
    {
        List<Entry<K, V>> bucket = table[hash(key)];

        if (bucket == null)
        {
            return null;
        }

        Iterator<Entry<K, V>> bucketIterator = bucket.iterator();
        V oldValue = null;

        while (bucketIterator.hasNext())
        {
            Entry<K, V> entry = bucketIterator.next();

            if (keysEqual(key, entry.getKey()))
            {
                oldValue = entry.getValue();
                bucketIterator.remove();
                size--;
                break;
            }
        }

        if (bucket.isEmpty())
        {
            table[hash(key)] = null;
        }

        return oldValue;
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public int size()
    {
        return size;
    }

    public boolean containsKey(K key)
    {
        boolean containsKey = false;

        if (!isEmpty())
        {
            List<Entry<K, V>> bucket = table[hash(key)];


            for (Entry<K, V> entry : bucket)
            {
                if (entry.getKey() == key)
                {
                    containsKey = true;
                }
            }
        }

        return containsKey;
    }

    public boolean containsValue(V value)
    {
        boolean containsValue = false;

        if (!isEmpty())
        {
            for (List<Entry<K, V>> bucket : table)
            {
                if (bucket == null)
                {
                    continue;
                }

                for (Entry<K, V> entry : bucket)
                {
                    if (entry.getValue() == value)
                    {
                        containsValue = true;
                    }
                }
            }
        }
        return containsValue;
    }
}
