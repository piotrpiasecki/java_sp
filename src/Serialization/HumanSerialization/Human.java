package Serialization.HumanSerialization;

import java.io.*;
import java.util.Calendar;

public class Human implements Serializable
{
    private String name;
    private int age;
    private static Calendar calendar = Calendar.getInstance();

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        Human human = new Human("Andrzej", 22);

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("human.bin")))
        {
            objectOutputStream.writeObject(human);
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("human.bin")))
        {

            calendar.set(2019, 1, 1);

            Human readHuman = (Human) objectInputStream.readObject();

            System.out.println(readHuman.getName());
            System.out.println(readHuman.getAge());
        }
    }

    public Human(String name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public String getName()
    {
        return name;
    }

    public int getAge()
    {
        return age;
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
        stream.defaultReadObject();
//        age = stream.readInt() + Calendar.getInstance().get(Calendar.YEAR);
//        age = stream.readInt() + Calendar.YEAR;
        age = stream.readInt() + calendar.get(Calendar.YEAR);

    }

    private void writeObject(ObjectOutputStream stream) throws IOException
    {
        stream.defaultWriteObject();
//        stream.writeInt(age - Calendar.getInstance().get(Calendar.YEAR));
//        stream.writeInt(age - Calendar.YEAR);
        stream.writeInt(age - calendar.get(Calendar.YEAR));
    }
}
