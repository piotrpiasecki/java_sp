package Serialization;

import java.io.*;

public class SerializationExercise1
{
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        String path = "C:\\Users\\piase\\Desktop\\Programowanie\\java_SP\\SP_Exercises\\objects_file.bin";

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(path)))
        {
            outputStream.writeObject(Integer.valueOf(100));
            outputStream.writeObject(Integer.valueOf(200));
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path)))
        {
            System.out.println(objectInputStream.readObject());
            System.out.println(((Integer) objectInputStream.readObject()).toString());
        }
    }
}
