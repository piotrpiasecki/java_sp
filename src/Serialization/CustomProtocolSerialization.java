package Serialization;

import java.io.*;

public class CustomProtocolSerialization implements Externalizable
{
    private String field;

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        CustomProtocolSerialization customProtocolSerialization = new CustomProtocolSerialization("field value");

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("externalizable.bin")))
        {
           objectOutputStream.writeObject(customProtocolSerialization);
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("externalizable.bin")))
        {
            CustomProtocolSerialization readObject = (CustomProtocolSerialization) objectInputStream.readObject();
            System.out.println(readObject.field);
        }
    }

    public CustomProtocolSerialization()
    {
    }

    public CustomProtocolSerialization(String field)
    {
        this.field = field;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException
    {
        out.writeUTF(field);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
    {
        field = in.readUTF();
    }
}
