package Serialization.CarSerialization;

import java.io.Serializable;

public class Tyre implements Serializable
{
    private int size;

    public Tyre(int size)
    {
        this.size = size;
    }

    public int getSize()
    {
        return size;
    }
}
