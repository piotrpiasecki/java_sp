package Serialization.CarSerialization;

import java.io.Serializable;

public class Engine implements Serializable
{
//    private transient String model;
    private String model;

    public Engine(String model)
    {
        this.model = model;
    }

    public String getModel()
    {
        return model;
    }
}
