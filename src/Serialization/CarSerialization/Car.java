package Serialization.CarSerialization;

import java.io.Serializable;

public class Car implements Serializable
{
    private Engine engine;
    private Tyre[] tyres;

    public Car(Engine engine, Tyre[] tyres)
    {
        this.engine = engine;
        this.tyres = tyres;
    }

    public Engine getEngine()
    {
        return engine;
    }

    public Tyre[] getTyres()
    {
        return tyres;
    }
}
