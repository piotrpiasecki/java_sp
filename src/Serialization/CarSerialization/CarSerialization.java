package Serialization.CarSerialization;

import java.io.*;

public class CarSerialization
{
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        Tyre[] tyres = new Tyre[] {new Tyre(16), new Tyre(16), new Tyre(16), new Tyre(16)};
        Engine engine = new Engine("some model");
        Car serializedCar = new Car(engine, tyres);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("object-graph.bin"))) {
            outputStream.writeObject(serializedCar);
        }

        Car deserializedCar = null;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("object-graph.bin"))) {
            deserializedCar = (Car) inputStream.readObject();
            System.out.println(deserializedCar.getEngine().getModel());
            System.out.println(deserializedCar.getTyres().length);
        }

        System.out.println(serializedCar == deserializedCar);
        System.out.println(serializedCar.equals(deserializedCar));
    }
}
